
## 项目介绍

* 点点教程的网站源码

    模仿 https://www.javatpoint.com/ 用于分享文章教程

## 主要技术
    
* rust + web-actix + tera + diesel + mysql

* Dot2Editor markdown编辑器

    https://github.com/noxue/Dot2Editor

    * 支持文件上传，支持七牛云，支持表格

## 如何运行

* 方式一：编译源码安装
    - rust项目，懂的都懂

* 方式二：无需另外安装任何软件(有手就行)
    1. 下载 https://gitee.com/dot2/tutorial/attach_files/572084/download/tutorial.zip
    2. 解压后，配置 .env 文件，（**如果配置自己的数据库，需要导入tutorial.sql 文件。** 我默认提供了远程mysql测试数据库，可以不修改任何配置，也可能随时关闭测试服务器）
    3. 启动项目
        - windows 
            直接双击运行 tutorial.exe即可
        - linux
            进入目录后运行 `nohup ./tutorial &`
        
* 后台地址
  https://blog.noxue.com/admin/ (**注意**，最后的 / 必须加上)


## 项目演示地址

https://blog.noxue.com


## 对应的微信小程序(前端已完善，目前未对接后端)

https://gitee.com/dot2/mini-tutorial

## 其他提示

* 修改html模板后，需要重启程序
* 后台添加文章，上传图片或者文件可以直接拖拽到编辑器，或者粘贴截图
* 演示数据中没包含图片文件，所以默认前台课程图片文件加载失败，请自行在后台修改课程图标。
* 如果一个可见课程都没有的情况下会前台会报错，因为一般不可能没有文章，所以直接写死。
