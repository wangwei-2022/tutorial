CREATE TABLE IF NOT EXISTS `tutorials` (
  `id` varchar(40) COLLATE utf8mb4_bin NOT NULL primary key,
  `pid` varchar(40) COLLATE utf8mb4_bin DEFAULT '',
  `tid` varchar(40) COLLATE utf8mb4_bin NOT NULL COMMENT '分类编号',
  `title` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '教程标题',
  `rank` int DEFAULT '0' COMMENT '排序编号，数字越大越靠前，默认 0',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '描述信息，用于seo',
  `keyword` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '关键词，用于seo',
  `content` text COLLATE utf8mb4_bin NOT NULL COMMENT '教程内容',
  `hidden` tinyint(1) DEFAULT '1' COMMENT '是否隐藏，true隐藏',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='教程表';

ALTER TABLE `tutorials`
  ADD CONSTRAINT `tutorials_ibfk_1` FOREIGN KEY (`tid`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
  