var isMenuHidden = true; // 默认菜单隐藏
function switchMenu() {
    var menuBox = document.getElementById("menuBox");
    var menuBtn = document.getElementById("menuBtn");
    if (isMenuHidden) {
        menuBox.className = "tutorials show"
        menuBtn.className = "menuBtn"
    } else {
        menuBox.className = "tutorials"
        menuBtn.className = "menuBtn show"
    }

    isMenuHidden = !isMenuHidden;
}

window.onload = function() {
    var ts = document.getElementsByClassName("tutorials")[0].getElementsByTagName("a");

    var curId = 0; // 当前是前端目录列表的第几个文章
    for (var n = 0; n < ts.length; n++) {
        var t = ts[n];
        //找到当前文章在菜单列表中的下标索引，从 0 开始
        if (t.classList.value.indexOf("selected") != -1) {
            curId = n;
            break;
        }
    }

    // 获取上一篇下一篇的节点,a标签，用于修改他们的href
    var prevNode = document.getElementById("prev_tutorial")
    var nextNode = document.getElementById("next_tutorial")

    // 第一篇和最后一篇隐藏相应的跳转链接
    if (curId == 0) {
        prevNode.style.display = "none";
    }
    if (curId == ts.length - 1) {
        nextNode.style.display = "none";
    }

    // debugger
    prevNode.href = ts[curId - 1];
    nextNode.href = ts[curId + 1];

}